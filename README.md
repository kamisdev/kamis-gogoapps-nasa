# How to run

### METHOD - 1 - Dockerfile

docker build -t kamis_gogo_nasa .<br>
docker run -d -p 8080:8080 kamis_gogo_nasa

### METHOD - 2 - docker-compose

docker-compose config<br>
docker-compose up --build -d

# After Run and Test
go test ./...