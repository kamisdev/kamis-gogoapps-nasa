package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/araddon/dateparse"
)

func HandlerGetPictures(w http.ResponseWriter, r *http.Request) {

	if r.Method != "GET" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(ErrorResponse{
			Err: "Invalid Request Method",
		})
		return
	}

	var urlList []string

	paramFrom := r.FormValue("from")
	paramTo := r.FormValue("to")

	startDate, err := dateparse.ParseLocal(paramFrom)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(ErrorResponse{
			Err: err.Error(),
		})
		return
	}

	endDate, err := dateparse.ParseLocal(paramTo)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(ErrorResponse{
			Err: err.Error(),
		})
		return
	}

	if startDate.After(endDate) || startDate.After(time.Now()) {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(ErrorResponse{
			Err: "Invalid Params",
		})
		return
	}

	if endDate.After(time.Now()) {
		endDate = time.Now()
	}

	concurrencyLimit, err := strconv.Atoi(os.Getenv("CONCURRENT_REQUESTS"))
	// fmt.Fprintln(w, os.Getenv("CONCURRENT_REQUESTS"))
	if err != nil {
		concurrencyLimit = 5
	}

	api_key := os.Getenv("API_KEY")
	// fmt.Fprintln(w, os.Getenv("API_KEY"))

	if api_key == "" {
		api_key = "DEMO_KEY"
	}

	waitChan := make(chan struct{}, concurrencyLimit)

	curDatePointer := startDate.Add(-24 * time.Hour)

	out := make(chan error)

	var wg sync.WaitGroup
	wg.Add(int(endDate.Sub(startDate).Hours()/24.0) + 1)

	for {
		waitChan <- struct{}{}
		curDatePointer = curDatePointer.Add(24 * time.Hour)

		if curDatePointer.After(endDate) {
			wg.Wait()
			break
		}

		go func(curDate time.Time) {

			urlDate := curDate.String()
			apiUrl := fmt.Sprintf("https://api.nasa.gov/planetary/apod?api_key=%s&date=%s", api_key, urlDate[0:10])
			urlList = append(urlList, GetUrl(apiUrl, r.Context(), out))
			<-waitChan
			wg.Done()

		}(curDatePointer)

		go func() {
			if err := (<-out); err != nil {
				fmt.Println(err.Error())
			}
		}()
	}

	output := OutResponse{
		Urls: urlList,
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(output)
}

func GetUrl(apiUrl string, ctx context.Context, out chan error) string {
	fmt.Println(apiUrl, "begin doing something")

	wCtx, cancel := context.WithCancel(ctx)

	req, err := http.NewRequestWithContext(
		wCtx,
		"GET",
		apiUrl,
		nil,
	)
	if err != nil {
		fmt.Println(apiUrl, "creating request error")
		out <- err
		cancel()
		return err.Error()
	}

	rsp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(apiUrl, err.Error())
		out <- err
		cancel()
		return err.Error()
	} else if rsp.StatusCode != http.StatusOK {
		fmt.Println(apiUrl, "response status error")
		out <- fmt.Errorf("%d: %s", rsp.StatusCode, rsp.Status)
		cancel()
		return fmt.Errorf("%d: %s", rsp.StatusCode, rsp.Status).Error()
	}

	defer rsp.Body.Close()

	var imgInfo NasaResponse

	if err := json.NewDecoder(rsp.Body).Decode(&imgInfo); err != nil {
		fmt.Println(apiUrl, err.Error())
		out <- err
		cancel()
		return err.Error()
	}

	fmt.Println(apiUrl, "done")

	cancel()
	return imgInfo.Url
}
