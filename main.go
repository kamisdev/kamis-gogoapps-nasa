package main

import (
	"fmt"
	"net/http"
	"os"
)

func HandlerHello(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, "hello world!")
}

func main() {
	http.HandleFunc("/", HandlerHello)
	http.HandleFunc("/pictures", HandlerGetPictures)
	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}
	http.ListenAndServe(":"+httpPort, nil)
	fmt.Println("SERVER STARTED on PORT :", os.Getenv("HTTP_PORT"))
	fmt.Println("ENDPOINT Example :", "GET /pictures?from=2020-01-04&to=2020-02-05")
}
