package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/require"
)

func TestRespondHello(t *testing.T) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}
	resp, err := http.Get("http://localhost:" + os.Getenv("PORT") + "/")
	if err != nil {
		t.Log("not ready, waiting...")
	}

	require.NoError(t, err, "HTTP error")
	defer resp.Body.Close()

	require.Equal(t, http.StatusOK, resp.StatusCode, "HTTP status code")

	body, err := io.ReadAll(resp.Body)
	require.NoError(t, err, "failed to read HTTP body")

	require.Contains(t, string(body), "hello world!")
}

func TestRespondPictureFromToParamError(t *testing.T) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}
	resp, err := http.Get("http://localhost:" + os.Getenv("PORT") + "/" + "pictures?from=2022-01-01&to=2021-03-03")
	if err != nil {
		t.Log("not ready, waiting...")
	}

	require.NoError(t, err, "HTTP error")
	defer resp.Body.Close()

	require.Equal(t, http.StatusBadRequest, resp.StatusCode, "HTTP status code")

	body, err := io.ReadAll(resp.Body)
	require.NoError(t, err, "failed to read HTTP body")

	require.Contains(t, string(body), "Invalid Params")
}
