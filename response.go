package main

type NasaResponse struct {
	Copyright       string `json:"copyright"`
	Date            string `json:"date"`
	Explanation     string `json:"explanation"`
	HDUrl           string `json:"hdurl"`
	MediaType       string `json:"media_type"`
	Service_Version string `json:"service_version"`
	Title           string `json:"title"`
	Url             string `json:"url"`
}

type OutResponse struct {
	Urls []string `json:"urls"`
}

type ErrorResponse struct {
	Err string `json:"error"`
}
